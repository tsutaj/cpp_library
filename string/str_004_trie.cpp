// トライ木
// Vefiried: 天下一プログラマーコンテスト 2016 本戦 C: だんごたくさん
// Trie のポインタ配列は、適宜必要なぶんだけ縮小すること！

const int SIZE = 256;
struct Trie {
    Trie* node[SIZE];
    bool leaf;
    Trie() {
        leaf = false;
        fill(node, node+SIZE, (Trie *)0);
    }
    void insert(const string &s) {
        Trie* r = this;
        for(size_t i=0; i<s.length(); i++) {
            int c = s[i];
            if(!r -> node[c]) r -> node[c] = new Trie;
            r = r -> node[c];
        }
        r -> leaf = true;
    }
    // search (exact match)
    bool find(const string &s, int mode) {
        const Trie *r = this;
        for(size_t i=0; i<s.length(); i++) {
            int c = s[i];
            if(!r -> node[c]) return false;
            r = r -> node[c];
        }
        return r -> leaf;
    }
};